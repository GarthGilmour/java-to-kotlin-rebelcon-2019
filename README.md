# Live Coding Your Way From Java To Kotlin #

This repository contains the materials used for the **Live Coding Your Way From Java To Kotlin** workshop delivered at [Rebelcon](https://rebelcon.io) on 19th June 2019. The course was delivered by [Garth Gilmour]([https://twitter.com/GarthGilmour](https://twitter.com/GarthGilmour)) and [Eamonn Boyle]([https://twitter.com/BoyleEamonn](https://twitter.com/BoyleEamonn)) on behalf of [Instil Software]([https://instil.co/](https://instil.co/)).

The materials mostly consist of Kotlin projects built via Gradle build files. All of these projects were developed in [JetBrains IntelliJ Community Edition]([https://www.jetbrains.com/idea/download/](https://www.jetbrains.com/idea/download/)) and Java 8. There should be no issues with using in other configurations, although the removal of JVM modules from JDK 9 onwards may introduce classloader errors.

A slide deck describing the projects, their underlying architecture and the exercises / solutions was distributed during the workshop. However the table below provides an overview.

**NB** The syntax 'project-name-x' indicates there is both a 'project-name-start' and 'project-name-finish'

|    Folder                      | Project                     | Description                                                              |
|--------------------------------|-----------------------------|--------------------------------------------------------------------------|
| StandardProjects               | KotlinLanguageExamples      | Covers the basic syntax of Kotlin                                        |
| StandardProjects               | KotlinLanguageExercises     | Coding exercises to help hearn the Kotlin syntax                         |
| StandardProjects               | TornadoFxDemos              | Examples of JavaFX UIs written using the Tornado Kotlin DSL              |
| StandardProjects               | HelloArrowFx                | A simple example of the Arrow Fx library for pure FP coding with Effects |
| StandardProjects               | CoroutinesDemos             | Examples of the language and library support for coroutines in Kotlin    |
| StandardProjects               | GradleKotlinProjectReactor  | Demos of the Project Reactor library which underpins Spring 5            |
| StandardProjects               | SampleServices              | RESTful services used in the projects above	                          |
| WorkshopProjects/Iteration1    | iteration-one-client-x      | A sample TornadoFX UI                                                    |
| WorkshopProjects/Iteration1    | iteration-one-server-x      | A standard Spring MVC service                                            |
| WorkshopProjects/Iteration2    | iteration-two-client-x      | UI now uses the WebFlux WebClient                                        |
| WorkshopProjects/Iteration2    | iteration-two-server-x      | Service is now written in WebFlux                                        |
| WorkshopProjects/Iteration3    | iteration-three-client-x    | UI is similar to above	                                                  |
| WorkshopProjects/Iteration3    | iteration-three-server-x    | Service now uses Functional Endpoints                                    |
| WorkshopProjects/Iteration4    | iteration-four-client-x     | UI now uses coroutines to call server                                    |
| WorkshopProjects/Iteration4    | iteration-four-server-x     | Operations now delay before responding                                   |
| WorkshopProjects/Iteration5    | iteration-five-client       | UI is similar to above	                                                  |
| WorkshopProjects/Iteration5    | iteration-five-server       | Service is now written via Ktor                                          |
| WorkshopProjects/Iteration6    | iteration-six-client        | UI is console based via Arrow Fx                                         |
| WorkshopProjects/Iteration6    | iteration-six-server        | Service is again written in Spring                                       |
| WorkshopProjects/Iteration7    | iteration-seven-client      | UI is once again using TornadoFX                                         |
| WorkshopProjects/Iteration7    | iteration-seven-server      | Service is now written in HTTP4K                                         |
