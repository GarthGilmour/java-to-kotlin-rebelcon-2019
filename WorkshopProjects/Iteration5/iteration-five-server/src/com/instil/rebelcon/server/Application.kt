package com.instil.rebelcon

import io.ktor.application.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.http.*
import com.fasterxml.jackson.databind.*
import com.instil.rebelcon.server.model.Builder
import com.instil.rebelcon.server.model.Course
import io.ktor.jackson.*
import io.ktor.features.*
import io.ktor.http.HttpStatusCode.Companion.NotFound
import io.ktor.request.receive

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT)
        }
    }

    install(CORS) {
        method(HttpMethod.Options)
        method(HttpMethod.Put)
        method(HttpMethod.Delete)
        method(HttpMethod.Patch)
        header(HttpHeaders.Authorization)
        header("MyCustomHeader")
        allowCredentials = true
        anyHost() // @TODO: Don't do this in production if possible. Try to limit it.
    }

    val portfolio = Builder.buildPortfolio()

    routing {
        route("/courses") {
            get("/") {
                call.respond(portfolio.values)
            }

            get("/{id}") {
                val id = call.parameters["id"]
                val course = portfolio[id]
                if(course != null) {
                    call.respond(course)
                } else {
                    call.response.status(NotFound)
                    call.respondText("Course $id not found")
                }
            }

            put("/{id}") {
                val id = call.parameters["id"]
                val course = call.receive<Course>()
                if (id != null) {
                    portfolio[id] = course
                    call.respondText("Course $id added or updated")
                } else {
                    call.response.status(NotFound)
                    call.respondText("Course id not specified")
                }
            }

            delete("/{id}") {
                val id = call.parameters["id"]
                if(portfolio.containsKey(id)) {
                    portfolio.remove(id)
                    call.respondText("Course $id removed")
                } else {
                    call.response.status(NotFound)
                    call.respondText("Course $id not found")
                }
            }
        }
    }
}

