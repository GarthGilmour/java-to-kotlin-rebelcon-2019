package com.instil.rebelcon.client.gui

import com.instil.rebelcon.client.logic.CoursesController
import com.instil.rebelcon.client.model.CourseDifficulty
import com.instil.rebelcon.client.model.dto.Course
import com.instil.rebelcon.client.model.ui.FxCourse
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections.observableArrayList
import javafx.geometry.Pos
import javafx.scene.control.SelectionMode
import javafx.scene.layout.Priority
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import tornadofx.*

class CourseBookingView : View("Course Booking UI via Kotlin and TornadoFX") {
    //The controller which communicates with the server
    private val controller: CoursesController by inject()
    //The list of courses to be displayed (empty by default)
    private val courses = mutableListOf<Course>().asObservable()
    //A property for the current message to show the user
    private val currentMessage = SimpleStringProperty("")
    //The current course being edited (initially hidden)
    private val selectedCourse = FxCourse.default()
    //A property to control if the selected course should be shown
    private val showSelectedCourse = SimpleBooleanProperty(false)

    override val root = form {
        fieldset("Our Current Portfolio") {
            hbox(10) {
                button("Load All Courses Sync") {
                    action(::loadAllCoursesSync)
                }
                button("Load All Courses Async") {
                    action {
                        GlobalScope.launch(Dispatchers.Main) {
                            loadAllCoursesAsync()
                        }
                    }
                }
                button("Reset") {
                    action(::resetCourses)
                }
            }
            tableview(courses) {
                readonlyColumn("Course ID", Course::id)
                readonlyColumn("Course Title", Course::title).remainingWidth()
                readonlyColumn("Difficulty", Course::difficulty)
                readonlyColumn("Duration", Course::duration)
                smartResize()
                selectionModel.selectionMode = SelectionMode.SINGLE
                onSelectionChange(::newCourseSelected)
            }
        }
        fieldset("The Selected Course") {
            field("ID") {
                label().bind(selectedCourse.idProperty())
            }
            field("Title") {
                textfield(){
                    hgrow = Priority.ALWAYS
                }.bind(selectedCourse.titleProperty())
            }
            field("Difficulty") {
                combobox(selectedCourse.difficultyProperty()) {
                    items = observableArrayList(CourseDifficulty.values().map { it.toString() })
                }
            }
            field("Duration") {
                combobox(selectedCourse.durationProperty()) {
                    items = observableArrayList(1,2,3,4,5)
                }
            }
            hbox(10) {
                button("Update") {
                    action(::updateCourse)
                }
                button("Delete") {
                    action {
                        deleteCourse(selectedCourse.id)
                    }
                }
                alignment = Pos.BASELINE_RIGHT
            }
        }.visibleWhen(showSelectedCourse)
        fieldset("Messages") {
            label(currentMessage)
        }
    }

    private fun changeMessage(text: String) {
        currentMessage.value = text
    }

    private fun newCourseSelected(selected: Course?) {
        if (selected != null) {
            val result = controller.loadSingleCourse(selected.id)
            if (result != null) {
                displaySelectedCourse(result)
            } else {
                changeMessage("Course ${selected.id} not loaded!")
            }
        } else {
            changeMessage("No course currently selected")
        }
    }

    private fun displaySelectedCourse(course: Course) {
        selectedCourse.reset(course)
        showSelectedCourse.value = true
    }

    private fun updateCourse() {
        changeMessage(controller.updateCourse(selectedCourse.toDTO()))
        loadAllCoursesSync()
    }

    private fun deleteCourse(id: String) {
        changeMessage(controller.deleteCourse(id))
        loadAllCoursesSync()
    }

    private fun loadAllCoursesSync() {
        resetCourses()
        courses.addAll(controller.loadAllCoursesSync())
        showSelectedCourse.value = false
    }

    private fun resetCourses() {
        courses.clear()
    }

    private suspend fun loadAllCoursesAsync() {
        resetCourses()
        controller.loadAllCoursesAsync(courses)
        showSelectedCourse.value = false
    }
}