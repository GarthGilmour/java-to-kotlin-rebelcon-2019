package com.instil.rebelcon.server.controllers

import com.instil.rebelcon.server.DeletionException
import com.instil.rebelcon.server.model.Course
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.annotation.Resource

import org.springframework.http.HttpStatus.NO_CONTENT
import org.springframework.http.HttpStatus.OK
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity.notFound
import org.springframework.http.ResponseEntity.ok
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Duration

@RestController
@RequestMapping("/courses")
@Resource(name="portfolio")
class CoursesController(val portfolio: MutableMap<String, Course>) {

    @GetMapping(produces = [MediaType.TEXT_EVENT_STREAM_VALUE])
    fun allCourses(): ResponseEntity<Flux<Course>>  {
        return if (portfolio.isEmpty()) {
            notFound().build()
        } else {
            val coursesFlux = Flux
                    .fromIterable(portfolio.values)
                    .delayElements(Duration.ofSeconds(1))
            ok(coursesFlux)
        }
    }

    @PutMapping(value = ["/{id}"], consumes = ["application/json"], produces=[MediaType.TEXT_PLAIN_VALUE])
    fun addOrUpdateCourse(@RequestBody mono: Mono<Course>): Mono<String> {
        return mono.map { course ->
            portfolio[course.id] = course
            "Course updated"
        }
    }

    @GetMapping(value = ["/{id}"], produces = ["application/json"])
    fun singleCourse(@PathVariable("id") id : String): ResponseEntity<Mono<Course>> {
        val course  = portfolio[id]
        return if (course != null) {
            ok(Mono.just(course))
        } else {
            notFound().build()
        }
    }

    @DeleteMapping(value = ["/{id}"], produces=[MediaType.TEXT_PLAIN_VALUE])
    fun deleteById(@PathVariable("id") id : String) : ResponseEntity<Mono<String>>  {
        val course = portfolio[id]
        return if (course != null) {
            if (course.title.contains("Scala")) {
                throw DeletionException("Cannot remove Scala courses!");
            }
            portfolio.remove(id)
            ok(Mono.just("Removed $id"))
        } else {
            notFound().build()
        }
    }
}
