package com.instil.rebelcon.server

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class IterationTwoServerApplication

fun main(args: Array<String>) {
	runApplication<IterationTwoServerApplication>(*args)
}
