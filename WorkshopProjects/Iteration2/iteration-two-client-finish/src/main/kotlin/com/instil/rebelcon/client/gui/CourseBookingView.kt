package com.instil.rebelcon.client.gui

import com.instil.rebelcon.client.logic.CoursesController
import com.instil.rebelcon.client.model.CourseDifficulty
import com.instil.rebelcon.client.model.dto.Course
import com.instil.rebelcon.client.model.ui.FxCourse
import javafx.application.Platform
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections.observableArrayList
import javafx.geometry.Pos
import javafx.scene.control.SelectionMode
import javafx.scene.layout.Priority
import tornadofx.*
import java.time.format.DateTimeFormatter.ofLocalizedTime
import java.time.format.FormatStyle

class CourseBookingView : View("Course Booking UI via Kotlin and TornadoFX") {
    //The controller which communicates with the server
    private val controller: CoursesController by inject()
    //The list of courses to be displayed (empty by default)
    private val courses = mutableListOf<Course>().asObservable()
    //A property for the current message to show the user
    private val currentMessage = SimpleStringProperty("")
    //The current course being edited (initially hidden)
    private val selectedCourse = FxCourse.default()
    //A property to control if the selected course should be shown
    private val showSelectedCourse = SimpleBooleanProperty(false)
    //A property to filter courses by difficulty
    private val currentDifficulty = SimpleStringProperty(CourseDifficulty.values()[0].toString())


    override val root = form {
        fieldset("Our Current Portfolio") {
            button("Load All Courses") {
                action(::loadAllCourses)
            }
            hbox(alignment = Pos.BASELINE_LEFT) {
                field("Find courses by difficulty") {
                    combobox(property = currentDifficulty) {
                        items = observableArrayList(CourseDifficulty.values().map { it.toString() })
                    }
                }
                button("Go") {
                    action(::loadCoursesByDifficulty)
                }
            }
            tableview(courses) {
                readonlyColumn("Course ID", Course::id)
                readonlyColumn("Course Title", Course::title).remainingWidth()
                readonlyColumn("Difficulty", Course::difficulty)
                readonlyColumn("Duration", Course::duration)
                smartResize()
                selectionModel.selectionMode = SelectionMode.SINGLE
                onSelectionChange(::newCourseSelected)
            }
        }
        fieldset("The Selected Course") {
            field("ID") {
                label().bind(selectedCourse.idProperty())
            }
            field("Title") {
                textfield() {
                    hgrow = Priority.ALWAYS
                }.bind(selectedCourse.titleProperty())
            }
            field("Difficulty") {
                combobox(selectedCourse.difficultyProperty()) {
                    items = observableArrayList(CourseDifficulty.values().map { it.toString() })
                }
            }
            field("Duration") {
                combobox(selectedCourse.durationProperty()) {
                    items = observableArrayList(1, 2, 3, 4, 5)
                }
            }
            hbox(10) {
                button("Update") {
                    action(::updateCourse)
                }
                button("Delete") {
                    action {
                        deleteCourse(selectedCourse.id)
                    }
                }
                alignment = Pos.BASELINE_RIGHT
            }
        }.visibleWhen(showSelectedCourse)
        fieldset("Messages") {
            label(currentMessage)
        }
    }

    private fun changeMessage(text: String) {
        Platform.runLater {
            currentMessage.value = text
        }
    }

    private fun newCourseSelected(selected: Course?) {
        if (selected != null) {
            controller.loadSingleCourse(selected.id) { course ->
                Platform.runLater {
                    displaySelectedCourse(course)
                }
            }
        }
    }

    private fun displaySelectedCourse(course: Course) {
        selectedCourse.reset(course)
        showSelectedCourse.value = true
        changeMessage("Displaying ${course.id}")
    }

    private fun updateCourse() {
        controller.updateCourse(selectedCourse.toDTO()) { message ->
            loadAllCourses()
            changeMessage(message)
        }
    }

    private fun deleteCourse(id: String) {
        fun success(message: String) {
            loadAllCourses()
            changeMessage(message)
        }

        fun failure(error: Throwable) {
            val msg = error.message ?: "No error message"
            changeMessage("Deletion failed with $msg")
        }

        controller.deleteCourse(id, ::success, ::failure)
    }

    private fun loadAllCourses() {
        courses.clear()
        controller.loadAllCourses { course ->
            Platform.runLater {
                courses.add(course)
            }
        }
        showSelectedCourse.value = false
    }

    private fun loadCoursesByDifficulty() {
        fun difficulty() = CourseDifficulty.valueOf(currentDifficulty.value)

        courses.clear()
        controller.loadCoursesByDifficulty(difficulty()) { pair ->
            val time = pair.second.format(ofLocalizedTime(FormatStyle.MEDIUM))
            Platform.runLater {
                courses.add(pair.first)
                changeMessage("Last course retrieved at $time")
            }
        }
        showSelectedCourse.value = false
    }
}