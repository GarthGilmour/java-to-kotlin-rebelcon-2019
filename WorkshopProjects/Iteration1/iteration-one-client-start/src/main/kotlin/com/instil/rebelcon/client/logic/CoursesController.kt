package com.instil.rebelcon.client.logic

import com.instil.rebelcon.client.model.dto.Course
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpMethod
import org.springframework.web.client.RestTemplate
import tornadofx.Controller
import org.springframework.http.HttpEntity

class CoursesController : Controller() {
    private val restTemplate = RestTemplate()
    private val baseURL = "http://localhost:8080/courses/"

    fun loadAllCourses(): List<Course> {
        val response = restTemplate.exchange(
            baseURL,
            HttpMethod.GET,
            null,
            object : ParameterizedTypeReference<List<Course>>() {})
        return response.body
    }

    fun loadSingleCourse(id: String): Course? {
        val url = "$baseURL$id"
        return restTemplate.getForObject(url, Course::class.java)
    }

    fun deleteCourse(id: String) {
        val url = "$baseURL$id"
        return restTemplate.delete(url)
    }

    fun updateCourse(course: Course) {
        val url = "$baseURL${course.id}"
        val entity = HttpEntity(course)
        restTemplate.exchange(url, HttpMethod.PUT, entity, Void::class.java)
    }
}