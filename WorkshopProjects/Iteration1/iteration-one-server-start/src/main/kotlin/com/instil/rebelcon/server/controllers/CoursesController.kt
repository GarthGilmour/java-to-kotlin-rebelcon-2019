package com.instil.rebelcon.server.controllers

import com.instil.rebelcon.server.DeletionException
import com.instil.rebelcon.server.model.Course
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.annotation.Resource

import org.springframework.http.HttpStatus.NO_CONTENT
import org.springframework.http.HttpStatus.OK
import org.springframework.http.ResponseEntity.notFound
import org.springframework.http.ResponseEntity.ok

@RestController
@RequestMapping("/courses")
@Resource(name="portfolio")
class CoursesController(val portfolio: MutableMap<String, Course>) {

    @GetMapping(produces = ["application/json"])
    fun allCourses(): ResponseEntity<Collection<Course>>  {
        return if (portfolio.isEmpty()) {
            notFound().build()
        } else {
            ok(portfolio.values)
        }
    }

    @ResponseStatus(NO_CONTENT)
    @PutMapping(value = ["/{id}"], consumes = ["application/json"])
    fun addOrUpdateCourse(@RequestBody newCourse: Course) {
        portfolio[newCourse.id] = newCourse
    }

    @GetMapping(value = ["/{id}"], produces = ["application/json"])
    fun singleCourse(@PathVariable("id") id : String): ResponseEntity<Course> {
        val course  = portfolio[id]
        return if (course != null) {
            ok(course)
        } else {
            notFound().build()
        }
    }

    @DeleteMapping(value = ["/{id}"])
    fun deleteById(@PathVariable("id") id : String) : ResponseEntity<String>  {
        val course = portfolio[id]
        return if (course != null) {
            if (course.title.contains("Scala")) {
                throw DeletionException("Cannot remove Scala courses!");
            }
            portfolio.remove(id);
            ok("[\"Removed $id\"]");
        } else {
            notFound().build();
        }
    }
}
