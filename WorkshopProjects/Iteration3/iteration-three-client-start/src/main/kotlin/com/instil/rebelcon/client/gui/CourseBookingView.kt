package com.instil.rebelcon.client.gui

import com.instil.rebelcon.client.logic.CoursesController
import com.instil.rebelcon.client.model.CourseDifficulty
import com.instil.rebelcon.client.model.dto.Course
import com.instil.rebelcon.client.model.ui.FxCourse
import javafx.application.Platform
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections.observableArrayList
import javafx.geometry.Pos
import javafx.scene.control.SelectionMode
import javafx.scene.layout.Priority
import tornadofx.*

class Item(val number: Int, private val title: String ) {
    override fun toString() = title
}

class CourseBookingView : View("Course Booking UI via Kotlin and TornadoFX") {
    //The controller which communicates with the server
    private val controller: CoursesController by inject()
    //The list of courses to be displayed (empty by default)
    private val courses = mutableListOf<Course>().asObservable()
    //The list of queries to be completed
    private val exercises = exerciseDescriptions().asObservable()
    private val currentExercise = SimpleObjectProperty(exercises[0])
    //A property for the current message to show the user
    private val currentMessage = SimpleStringProperty("")
    //The current course being edited (initially hidden)
    private val selectedCourse = FxCourse.default()
    //A property to control if the selected course should be shown
    private val showSelectedCourse = SimpleBooleanProperty(false)

    private fun exerciseDescriptions() = mutableListOf(
        Item(1,"Delete the first course from the list"),
        Item(2,"Sum the durations of beginners courses"),
        Item(3,"Display the IDs of advanced courses")
    )

    override val root = form {
        fieldset("Sample Queries") {
            combobox(currentExercise) {
                items = exercises

            }
            button("Call") {
                action(::attemptExercise)
            }
        }
        fieldset("Our Current Portfolio") {
            button("Load All Courses") {
                action(::loadAllCourses)
            }
            tableview(courses) {
                readonlyColumn("Course ID", Course::id)
                readonlyColumn("Course Title", Course::title).remainingWidth()
                readonlyColumn("Difficulty", Course::difficulty)
                readonlyColumn("Duration", Course::duration)
                smartResize()
                selectionModel.selectionMode = SelectionMode.SINGLE
                onSelectionChange(::newCourseSelected)
            }
        }
        fieldset("The Selected Course") {
            field("ID") {
                label().bind(selectedCourse.idProperty())
            }
            field("Title") {
                textfield() {
                    hgrow = Priority.ALWAYS
                }.bind(selectedCourse.titleProperty())
            }
            field("Difficulty") {
                combobox(selectedCourse.difficultyProperty()) {
                    items = observableArrayList(CourseDifficulty.values().map { it.toString() })
                }
            }
            field("Duration") {
                combobox(selectedCourse.durationProperty()) {
                    items = observableArrayList(1, 2, 3, 4, 5)
                }
            }
            hbox(10) {
                button("Update") {
                    action(::updateCourse)
                }
                button("Delete") {
                    action {
                        deleteCourse(selectedCourse.id)
                    }
                }
                alignment = Pos.BASELINE_RIGHT
            }
        }.visibleWhen(showSelectedCourse)
        fieldset("Messages") {
            label(currentMessage)
        }
    }

    private fun changeMessage(text: String) {
        Platform.runLater {
            currentMessage.value = text
        }
    }

    private fun newCourseSelected(selected: Course?) {
        if (selected != null) {
            controller.loadSingleCourse(selected.id) { course ->
                Platform.runLater {
                    displaySelectedCourse(course)
                }
            }
        }
    }

    private fun displaySelectedCourse(course: Course) {
        selectedCourse.reset(course)
        showSelectedCourse.value = true
        changeMessage("Displaying ${course.id}")
    }

    private fun updateCourse() {
        controller.updateCourse(selectedCourse.toDTO()) { message ->
            loadAllCourses()
            changeMessage(message)
        }
    }

    private fun deleteCourse(id: String) {
        fun success(message: String) {
            loadAllCourses()
            changeMessage(message)
        }

        fun failure(error: Throwable) {
            val msg = error.message ?: "No error message"
            changeMessage("Deletion failed with $msg")
        }

        controller.deleteCourse(id, ::success, ::failure)
    }

    private fun loadAllCourses() {
        courses.clear()
        controller.loadAllCourses { course ->
            Platform.runLater {
                courses.add(course)
            }
        }
        showSelectedCourse.value = false
    }

    private fun attemptExercise() {
        when(val number = currentExercise.value.number) {
            1,2,3 -> {
                changeMessage("Attempting exercise $number")
            }
            else -> {
                changeMessage("Unknown exercise!")
            }
        }
    }
}