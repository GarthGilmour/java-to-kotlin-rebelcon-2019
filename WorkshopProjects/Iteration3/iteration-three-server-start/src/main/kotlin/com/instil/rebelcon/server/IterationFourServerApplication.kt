package com.instil.rebelcon.server

import com.instil.rebelcon.server.model.Course
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.http.MediaType
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.ServerResponse.*
import org.springframework.web.reactive.function.server.router
import org.springframework.web.reactive.function.BodyInserters.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Duration
import javax.annotation.Resource

@SpringBootApplication
@Resource(name = "portfolio")
class IterationFourServerApplication(val portfolio: MutableMap<String, Course>) {

    @Bean
    fun routes() = router {
        ("/courses" and accept(APPLICATION_JSON)).nest {
            GET("/", ::allCourses)
            GET("/{id}", ::singleCourse)
            PUT("/{id}", ::addOrUpdateCourse)
            DELETE("/{id}", ::deleteCourse)
        }
    }

    fun allCourses(request: ServerRequest): Mono<ServerResponse> {
        return if (portfolio.isEmpty()) {
            notFound().build()
        } else {
            val coursesFlux = Flux
                    .fromIterable(portfolio.values)
                    .delayElements(Duration.ofSeconds(1))
            ok().contentType(MediaType.TEXT_EVENT_STREAM)
                    .body(fromPublisher(coursesFlux, Course::class.java))
        }
    }

    fun addOrUpdateCourse(request: ServerRequest): Mono<ServerResponse> {
        return request
                .bodyToMono(Course::class.java)
                .flatMap { course ->
                    portfolio[course.id] = course
                    ok().body(fromObject("Course Updated"))
                }
    }

    fun singleCourse(request: ServerRequest): Mono<ServerResponse> {
        val id = request.pathVariable("id")
        val course = portfolio[id]
        return if (course != null) {
            ok().body(fromObject(course))
        } else {
            notFound().build()
        }
    }

    private fun removeCourse(course: Course): String {
        portfolio.remove(course.id)
        return "Removed ${course.id}"
    }

    fun deleteCourse(request: ServerRequest): Mono<ServerResponse> {
        val id = request.pathVariable("id")
        val course = portfolio[id]
        return if (course != null) {
            ok().body(fromObject(removeCourse(course)))
        } else {
            notFound().build()
        }
    }
}

fun main(args: Array<String>) {
    runApplication<IterationFourServerApplication>(*args)
}
