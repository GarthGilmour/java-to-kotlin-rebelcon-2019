package functions.coding.the.functional.toolkit

fun main() {
    val originalData = arrayListOf("12", "34", "56", "78", "90")
    val mappedData = map(originalData, String::toInt)
    val filteredData = filter(mappedData) { it > 50 }
    val partitionedData = partition(mappedData) { it > 50 }

    printResults("Results of mapping", mappedData)
    printResults("Results of filtering", filteredData)
    printResults("Results of partitioning", partitionedData)
}

fun <T> printResults(title: String, input: List<T>) {
    println("----- $title -----")
    for (item in input) {
        println("\t$item")
    }
}
fun <T, U> printResults(title: String, input: Pair<List<T>, List<U>>) {
    println("----- $title -----")
    println("\tfirst items in pair")
    for (item in input.first) {
        println("\t\t$item")
    }
    println("\tsecond items in pair")
    for (item in input.second) {
        println("\t\t$item")
    }
}

fun <T, U> map(input: List<T>, mappingFunc: (T) -> U): List<U> {
    val results = ArrayList<U>()
    for (item in input) {
        results.add(mappingFunc(item))
    }
    return results
}
fun <T> filter(input: List<T>, filterFunc: (T) -> Boolean): List<T> {
    val results = ArrayList<T>()
    for (item in input) {
        if (filterFunc(item)) {
            results.add(item)
        }
    }
    return results
}
fun <T> partition(input: List<T>, filterFunc: (T) -> Boolean): Pair<List<T>, List<T>> {
    val results = Pair(ArrayList<T>(), ArrayList<T>())
    for (item in input) {
        if (filterFunc(item)) {
            results.first.add(item)
        } else {
            results.second.add(item)
        }
    }
    return results
}