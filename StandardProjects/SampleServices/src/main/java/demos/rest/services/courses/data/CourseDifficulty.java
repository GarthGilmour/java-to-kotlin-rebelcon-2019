package demos.rest.services.courses.data;

public enum CourseDifficulty {
    BEGINNER,
    INTERMEDIATE,
    ADVANCED
}
