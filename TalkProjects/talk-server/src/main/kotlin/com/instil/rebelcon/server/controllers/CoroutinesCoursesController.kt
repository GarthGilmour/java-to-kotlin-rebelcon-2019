package com.instil.rebelcon.server.controllers

import com.instil.rebelcon.server.DeletionException
import com.instil.rebelcon.server.model.Course
import com.instil.rebelcon.server.model.CourseDifficulty
import kotlinx.coroutines.delay
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.annotation.Resource

import org.springframework.http.HttpStatus.OK
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity.notFound
import org.springframework.http.ResponseEntity.ok

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

@RestController
@RequestMapping("/coroutines-courses")
@Resource(name = "portfolio")
class CoroutinesCoursesController(val portfolio: MutableMap<String, Course>) {

    @GetMapping(produces = [MediaType.TEXT_EVENT_STREAM_VALUE])
    fun allCourses(): ResponseEntity<Flow<Course>> {
        return if (portfolio.isEmpty()) {
            notFound().build()
        } else {
            val coursesFlow = flow {
                portfolio.values.forEach { course ->
                    delay(1000)
                    emit(course)
                }
            }
            ok(coursesFlow)
        }
    }

    @GetMapping(value = ["/byDifficulty/{difficulty}"], produces = [MediaType.TEXT_EVENT_STREAM_VALUE])
    fun coursesByDifficulty(@PathVariable("difficulty") difficulty: CourseDifficulty): ResponseEntity<Flow<Course>> {
        return if (portfolio.isEmpty()) {
            notFound().build()
        } else {
            val coursesFlow = flow {
                portfolio.values
                        .filter {
                            it.difficulty == difficulty
                        }
                        .forEach {
                            delay(1000)
                            emit(it)
                        }
            }
            ResponseEntity(coursesFlow, OK)
        }
    }

    @PutMapping(value = ["/{id}"], consumes = ["application/json"], produces = [MediaType.TEXT_PLAIN_VALUE])
    suspend fun addOrUpdateCourse(@RequestBody course: Course): String {
        portfolio[course.id] = course
        return "Course updated"
    }

    @GetMapping(value = ["/{id}"], produces = ["application/json"])
    suspend fun singleCourse(@PathVariable("id") id: String): ResponseEntity<Course> {
        val course = portfolio[id]
        return if (course != null) {
            ok(course)
        } else {
            notFound().build()
        }
    }

    @DeleteMapping(value = ["/{id}"], produces = [MediaType.TEXT_PLAIN_VALUE])
    suspend fun deleteById(@PathVariable("id") id: String): ResponseEntity<String> {
        val course = portfolio[id]
        return if (course != null) {
            if (course.title.contains("Scala")) {
                throw DeletionException("Cannot remove Scala courses!");
            }
            portfolio.remove(id)
            ok("Removed $id")
        } else {
            notFound().build()
        }
    }
}
